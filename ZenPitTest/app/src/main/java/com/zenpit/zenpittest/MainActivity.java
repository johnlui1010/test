package com.zenpit.zenpittest;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    //UI
    TextView tvIMEI, tvSerial, tvNFC, tvWiFi, tvInternet, tvDeveloperStatus, tvOSVersion, tvBattLevel;

    //Silent installation
    void InstallAPK_Silent_Android7(String Path) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Path = Path + ":Silent";
        String substring = Path.substring(Path.length() - 7, Path.length());
        intent.setDataAndType(Uri.fromFile(new File(Path)), "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    //Get IMEI information
    private String getIMEI() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            return telephonyManager.getDeviceId();
    }

    //Get NFC Status
    private boolean getNFCStatus() {
        NfcManager manager = (NfcManager) getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        if (adapter != null && adapter.isEnabled()) {
            return true;
        }
        return false;
    }

    //Determine the type of your internet connection
    private boolean getWiFiStatus(){
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null)
            return (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI);


        return false;
    }

    private static String getCurrentSsid(Context context) {
        String ssid = null;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();
            }
        }
        return ssid;
    }

    //Determine if you have an internet connection
    private boolean getInternetStatus() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    private boolean getDeveloperModeStatus() {
        int adb = Settings.Secure.getInt(this.getContentResolver(),
                Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, 0);
        return (adb==1);
    }

    private int getBatteryLevel(){
        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        return batLevel;
    }

    //After Android 6
    private boolean shouldAskPermission(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvIMEI = (TextView) findViewById(R.id.textView_imei);
        tvSerial = (TextView) findViewById(R.id.textView_serial);
        tvNFC = (TextView) findViewById(R.id.textView_nfcstatus);
        tvWiFi = (TextView) findViewById(R.id.textView_wifistatus);
        tvInternet = (TextView) findViewById(R.id.textView_internetstatus);
        tvDeveloperStatus = (TextView) findViewById(R.id.textView_developerstatus);
        tvOSVersion = (TextView) findViewById(R.id.textView_osversion);
        tvBattLevel = (TextView) findViewById(R.id.textView_battlevel);

        if (shouldAskPermission()) {
            final int MY_PERMISSIONS_REQUEST = 100;
            String[] permissos = {  Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.INSTALL_PACKAGES, Manifest.permission.READ_PHONE_STATE,
                                    Manifest.permission.CHANGE_WIFI_STATE};
            if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.INSTALL_PACKAGES) != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CHANGE_WIFI_STATE) != PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            permissos,
                            MY_PERMISSIONS_REQUEST);
            }
        }



        //String InstallAPK_Path = "/Download/StockCount.apk";
        //InstallAPK_Silent_Android7(Environment.getExternalStorageDirectory() +InstallAPK_Path);

    }

    @Override
    protected void onResume() {
        super.onResume();
        tvIMEI.setText("IMEI: " +getIMEI());
        tvSerial.setText("Serial Number: " + Build.SERIAL);
        tvNFC.setText("NFC Status: " + getNFCStatus());
        tvInternet.setText("Internet Status: " + getInternetStatus());
        if (getWiFiStatus())
            tvWiFi.setText("WiFi Status: Connected to " + getCurrentSsid(this));
        else
            tvWiFi.setText("WiFi Status: " + getWiFiStatus());
        tvDeveloperStatus.setText("Dev Mode Status: " +getDeveloperModeStatus());
        tvOSVersion.setText(("OS Version: " + android.os.Build.VERSION.RELEASE));
        tvBattLevel.setText("Batt Level: " + getBatteryLevel());
    }
}
